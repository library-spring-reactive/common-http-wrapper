package com.gitlab.residwi.library.common.swagger;

import com.gitlab.residwi.library.common.annotation.PagingRequestInQuery;
import com.gitlab.residwi.library.common.helper.PagingHelper;
import com.gitlab.residwi.library.common.model.request.PagingRequest;
import com.gitlab.residwi.library.common.model.response.PagingPayload;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@SpringBootTest(
        classes = SwaggerPagingRequestTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureWebTestClient
class SwaggerPagingRequestTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void testPaging() {
        webTestClient.get()
                .uri("/v3/api-docs")
                .exchange()
                .expectBody()
                .jsonPath("$.paths./paging.get.parameters[*].name").value(Matchers.hasItems("page", "item_per_page", "sort_by"))
                .jsonPath("$.paths./paging-request.get.parameters[*].name").value(Matchers.hasItems("page", "item_per_page", "sort_by"))
                .jsonPath("$.components.parameters.queryPagingRequestPage").isNotEmpty()
                .jsonPath("$.components.parameters.queryPagingRequestItemPerPage").isNotEmpty()
                .jsonPath("$.components.parameters.queryPagingRequestSortBy").isNotEmpty().returnResult();
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class PagingController {

            @PagingRequestInQuery
            @GetMapping(value = "/paging-request", produces = MediaType.APPLICATION_JSON_VALUE)
            public Mono<PagingRequest> pagingRequest(PagingRequest pagingRequest) {
                return Mono.just(pagingRequest);
            }

            @PagingRequestInQuery
            @GetMapping(value = "/paging", produces = MediaType.APPLICATION_JSON_VALUE)
            public Mono<PagingPayload> paging(PagingRequest pagingRequest) {
                return Mono.just(
                        PagingHelper.toPaging(pagingRequest, 100L, 100 * pagingRequest.getItemPerPage())
                );
            }

        }

    }
}
