package com.gitlab.residwi.library.common;

import com.gitlab.residwi.library.common.properties.PagingProperties;
import com.gitlab.residwi.library.common.resolver.PagingRequestArgumentResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.result.method.annotation.ArgumentResolverConfigurer;

@Configuration
@RequiredArgsConstructor
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@ConditionalOnClass(WebFluxConfigurer.class)
@AutoConfigureAfter(CommonAutoConfiguration.class)
public class CommonPagingRequestAutoConfiguration implements WebFluxConfigurer {

    private final PagingProperties pagingProperties;

    @Override
    public void configureArgumentResolvers(ArgumentResolverConfigurer configurer) {
        configurer.addCustomResolver(new PagingRequestArgumentResolver(pagingProperties));
    }
}
