package com.gitlab.residwi.library.common;

import com.gitlab.residwi.library.common.properties.PagingProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableConfigurationProperties({
        PagingProperties.class
})
@PropertySource(
        ignoreResourceNotFound = true,
        value = "classpath:library-common.properties"
)
public class CommonAutoConfiguration {

}
