package com.gitlab.residwi.library.common.resolver;

import com.gitlab.residwi.library.common.model.request.PagingRequest;
import com.gitlab.residwi.library.common.model.request.SortByRequest;
import com.gitlab.residwi.library.common.properties.PagingProperties;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@AllArgsConstructor
public class PagingRequestArgumentResolver implements HandlerMethodArgumentResolver {

    public static final String SORT_BY_SEPARATOR = ":";
    public static final String SORT_BY_SPLITTER = ",";
    public static final String EMPTY_STRING = "";

    private final PagingProperties pagingProperties;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return PagingRequest.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
        return Mono.fromSupplier(() -> fromServerHttpRequest(exchange.getRequest()));
    }

    private PagingRequest fromServerHttpRequest(ServerHttpRequest request) {
        var paging = new PagingRequest();

        paging.setPage(getLong(
                request.getQueryParams().getFirst(pagingProperties.getQuery().getPageKey()),
                pagingProperties.getDefaultPage()
        ));
        paging.setItemPerPage(getLong(
                request.getQueryParams().getFirst(pagingProperties.getQuery().getItemPerPageKey()),
                pagingProperties.getDefaultItemPerPage()
        ));

        if (paging.getItemPerPage() > pagingProperties.getMaxItemPerPage()) {
            paging.setItemPerPage(pagingProperties.getMaxItemPerPage());
        }
        paging.setSortBy(getSortByList(
                request.getQueryParams().getFirst(pagingProperties.getQuery().getSortByKey()),
                pagingProperties
        ));

        return paging;
    }

    private List<SortByRequest> getSortByList(String value, PagingProperties pagingProperties) {
        if (StringUtils.hasText(value)) {
            return toSortByList(value, pagingProperties);
        }
        return Collections.emptyList();
    }

    private Long getLong(String value, Long defaultValue) {
        if (value == null) {
            return defaultValue;
        } else {
            return toLong(value, defaultValue);
        }
    }

    private List<SortByRequest> toSortByList(String request, PagingProperties pagingProperties) {
        return Arrays.stream(request.split(SORT_BY_SPLITTER))
                .map(s -> toSortBy(s, pagingProperties))
                .filter(Objects::nonNull)
                .filter(sortBy -> Objects.nonNull(sortBy.getPropertyName()))
                .collect(Collectors.toList());
    }


    private SortByRequest toSortBy(String request, PagingProperties pagingProperties) {
        var sort = request.trim();
        if (!StringUtils.hasText(sort.replace(SORT_BY_SEPARATOR, EMPTY_STRING)) || sort.startsWith(SORT_BY_SEPARATOR)) {
            return null;
        }

        var sortBy = sort.split(SORT_BY_SEPARATOR);

        return new SortByRequest(
                getAt(sortBy, 0, null),
                EnumUtils.getEnum(
                        SortByRequest.SortType.class,
                        getAt(sortBy, 1, pagingProperties.getDefaultSortType().name()).toUpperCase()
                ));
    }

    public String getAt(String[] strings, int index, String defaultValue) {
        return strings.length <= index ? defaultValue : strings[index];
    }

    public Long toLong(String value, Long defaultValue) {
        try {
            return Long.valueOf(value);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }
}
