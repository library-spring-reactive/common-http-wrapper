package com.gitlab.residwi.library.common.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gitlab.residwi.library.common.model.request.SortByRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PagingPayload {

    private Long page;

    private Long totalPage;

    private Long itemPerPage;

    private Long totalItem;

    private List<SortByRequest> sortBy;
}
