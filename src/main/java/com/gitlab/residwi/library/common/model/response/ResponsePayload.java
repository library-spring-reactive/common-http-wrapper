package com.gitlab.residwi.library.common.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResponsePayload<T> {

    /**
     * Code , usually same as HTTP Code
     */
    private Integer code;

    /**
     * Status, usually same as HTTP status
     */
    private String status;

    /**
     * Response data
     */
    private T data;

    /**
     * Paging information, if response is paginate data
     */
    private PagingPayload paging;

    /**
     * Error information, if request is not valid
     */
    private Map<String, List<String>> errors;

    /**
     * Metadata information
     */
    private Map<String, Object> metadata;

}
