package com.gitlab.residwi.library.common.exception;

import com.gitlab.residwi.library.common.annotation.Metadatas;
import com.gitlab.residwi.library.common.model.response.ResponsePayload;
import org.slf4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.*;

public interface CommonErrorException {

    static Map<String, List<String>> from(BindingResult result, MessageSource messageSource) {
        return from(result, messageSource, Locale.getDefault());
    }

    static Map<String, List<String>> from(BindingResult result, MessageSource messageSource, Locale locale) {
        if (result.hasFieldErrors()) {
            var map = new HashMap<String, List<String>>();

            for (FieldError fieldError : result.getFieldErrors()) {
                var field = fieldError.getField();

                if (!map.containsKey(fieldError.getField())) {
                    map.put(field, new ArrayList<>());
                }

                var errorMessage = messageSource.getMessage(Objects.requireNonNull(fieldError.getCode()), fieldError.getArguments(), fieldError.getDefaultMessage(), locale);
                map.get(field).add(errorMessage);
            }

            return map;
        } else {
            return Collections.emptyMap();
        }
    }

    static Map<String, List<String>> from(Set<ConstraintViolation<?>> constraintViolations) {
        var map = new HashMap<String, List<String>>();

        constraintViolations.forEach(violation -> {
            for (String attribute : getAttributes(violation)) {
                putEntry(map, attribute, violation.getMessage());
            }
        });

        return map;
    }

    static void putEntry(Map<String, List<String>> map, String key, String value) {
        map.computeIfAbsent(key, k -> new ArrayList<>());
        map.get(key).add(value);
    }

    static String[] getAttributes(ConstraintViolation<?> constraintViolation) {
        var values = (String[]) constraintViolation.getConstraintDescriptor().getAttributes().get("path");
        if (values == null || values.length == 0) {
            return getAttributesFromPath(constraintViolation);
        } else {
            return values;
        }
    }

    static String[] getAttributesFromPath(ConstraintViolation<?> constraintViolation) {
        var path = constraintViolation.getPropertyPath();

        var builder = new StringBuilder();
        path.forEach(node -> {
            if (node.getName() != null) {
                if (builder.length() > 0) {
                    builder.append(".");
                }

                builder.append(node.getName());
            }
        });

        return new String[]{builder.toString()};
    }

    Logger getLogger();

    MessageSource getMessageSource();

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    default ResponsePayload<Object> httpMessageNotReadableException(HttpMessageNotReadableException e) {
        getLogger().warn(HttpMessageNotReadableException.class.getName(), e);
        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setStatus(HttpStatus.BAD_REQUEST.name());
        return response;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    default ResponsePayload<Object> throwable(Throwable e) {
        getLogger().error(e.getClass().getName(), e);
        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
        return response;
    }

    @ExceptionHandler(ServerWebInputException.class)
    default ResponseEntity<ResponsePayload<Object>> serverWebInputException(ServerWebInputException e) {
        getLogger().warn(ServerWebInputException.class.getName(), e);

        var errors = new HashMap<String, List<String>>();
        errors.put(Objects.requireNonNull(e.getMethodParameter()).getParameterName(), Collections.singletonList(e.getReason()));

        var response = new ResponsePayload<>();
        response.setCode(e.getStatus().value());
        response.setStatus(e.getStatus().name());
        response.setErrors(errors);

        return ResponseEntity.status(e.getStatus()).body(response);
    }

    @ExceptionHandler(WebExchangeBindException.class)
    default ResponseEntity<ResponsePayload<Object>> webExchangeBindException(WebExchangeBindException e) {
        getLogger().warn(WebExchangeBindException.class.getName(), e);

        var response = new ResponsePayload<>();
        response.setCode(e.getStatus().value());
        response.setStatus(e.getStatus().name());
        response.setErrors(from(e.getBindingResult(), getMessageSource()));

        return ResponseEntity.status(e.getStatus()).body(response);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    default ResponseEntity<ResponsePayload<Object>> constraintViolationException(ConstraintViolationException e) {
        getLogger().warn(ConstraintViolationException.class.getName(), e);

        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setStatus(HttpStatus.BAD_REQUEST.name());
        response.setErrors(from(e.getConstraintViolations()));
        response.setMetadata(Collections.singletonMap("errors", getMetaData(e.getConstraintViolations())));

        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(ResponseStatusException.class)
    default ResponseEntity<ResponsePayload<Object>> responseStatusException(ResponseStatusException e) {
        getLogger().warn(ResponseStatusException.class.getName(), e);
        var errors = new HashMap<String, List<String>>();
        errors.put("reason", Collections.singletonList(e.getReason()));

        var response = new ResponsePayload<>();
        response.setCode(e.getStatus().value());
        response.setStatus(e.getStatus().name());
        response.setErrors(errors);

        return ResponseEntity.status(e.getStatus()).body(response);
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(NotAcceptableStatusException.class)
    default ResponsePayload<Object> notAcceptableStatusException(NotAcceptableStatusException e) {
        getLogger().warn(NotAcceptableStatusException.class.getName(), e);
        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.NOT_ACCEPTABLE.value());
        response.setStatus(HttpStatus.NOT_ACCEPTABLE.name());
        return response;
    }

    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(UnsupportedMediaTypeStatusException.class)
    default ResponsePayload<Object> unsupportedMediaTypeStatusException(UnsupportedMediaTypeStatusException e) {
        getLogger().warn(UnsupportedMediaTypeStatusException.class.getName(), e);
        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
        response.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.name());
        return response;
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(MethodNotAllowedException.class)
    default ResponsePayload<Object> methodNotAllowedException(MethodNotAllowedException e) {
        getLogger().warn(MethodNotAllowedException.class.getName(), e);
        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.METHOD_NOT_ALLOWED.value());
        response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.name());
        return response;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServerErrorException.class)
    default ResponsePayload<Object> serverErrorException(ServerErrorException e) {
        getLogger().warn(ServerErrorException.class.getName(), e);
        var response = new ResponsePayload<>();
        response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
        return response;
    }

    default Map<String, Map<String, String>> getMetaData(Set<ConstraintViolation<?>> constraintViolations) {
        Map<String, Map<String, String>> metadata = new HashMap<>();
        constraintViolations.forEach(violation -> {
            try {
                var beanClass = violation.getLeafBean().getClass();

                var field = "";
                for (Path.Node node : violation.getPropertyPath()) {
                    field = node.getName();
                }

                var declaredField = beanClass.getDeclaredField(field);
                var metadatas = declaredField.getAnnotation(Metadatas.class);

                if (metadatas != null) {
                    var values = new HashMap<String, String>();

                    for (var metaData : metadatas.value()) {
                        values.put(metaData.key(), metaData.value());
                    }

                    for (var attribute : getAttributes(violation)) {
                        metadata.put(attribute, values);
                    }
                }

            } catch (Throwable throwable) {
                getLogger().warn(throwable.getMessage(), throwable);
            }
        });

        return metadata;
    }

}
