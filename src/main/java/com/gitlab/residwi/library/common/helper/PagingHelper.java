package com.gitlab.residwi.library.common.helper;

import com.gitlab.residwi.library.common.model.request.PagingRequest;
import com.gitlab.residwi.library.common.model.response.PagingPayload;
import lombok.experimental.UtilityClass;

@UtilityClass
public class PagingHelper {

    public static PagingPayload toPaging(PagingRequest request, Long totalItem) {
        var totalPage = totalItem / request.getItemPerPage();
        if (totalItem % request.getItemPerPage() != 0) {
            totalPage++;
        }
        return toPaging(request, totalPage, totalItem);
    }

    public static PagingPayload toPaging(PagingRequest request, Long totalPage, Long totalItem) {
        return PagingPayload.builder()
                .page(request.getPage())
                .totalItem(totalItem)
                .totalPage(totalPage)
                .itemPerPage(request.getItemPerPage())
                .sortBy(request.getSortBy())
                .build();
    }

}