package com.gitlab.residwi.library.common.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Metadatas {

    Metadata[] value() default {};
}
